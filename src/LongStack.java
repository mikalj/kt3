import java.util.EmptyStackException;
import java.util.LinkedList;

public class LongStack {
   private LinkedList<Long> stack;
   public static void main (String[] argum) {
      // TODO!!! Your tests here!
   }

   LongStack() {
     stack = new LinkedList<Long>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack cloned = new LongStack();
      for (int i = stack.size() - 1; i >= 0; i--) {
         cloned.push(stack.get(i));
      }
      return cloned;
   }

   public boolean stEmpty() {
      return stack.isEmpty();
   }

   public void push (long a) {
      stack.push(a);
   }

   public long pop() {
      if (!stack.isEmpty()) {
         return stack.pop();
      }
      throw new RuntimeException("Cannot pop from empty stack!");
   } // pop

   public void op (String s) {
      if (stack.size() < 2){
         throw new NullPointerException("Stack does not have enough elements for operation " + s + " !");
      }
      long secondElement = stack.pop();
      long firstElement = stack.pop();
      switch (s){
         case "+":
            push( firstElement + secondElement);
            break;
         case "-":
            push( firstElement - secondElement);
            break;
         case "*":
            push( firstElement * secondElement);
            break;
         case "/":
            if(secondElement == 0) {
               throw new RuntimeException("Cannot divide by" + 0  + "(second element was 0)!");
            }
            push( firstElement / secondElement);
            break;
         default:
            throw new RuntimeException("Illegal symbol " + s + " in expression" + stack.toString());
      }
   }

   public long tos() {
      if (!stack.isEmpty()) {
         return stack.getFirst();
      }
      throw new RuntimeException("Stack has no elements!");
   }

   @Override
   public boolean equals (Object o) {
      if (o == this){
         return true;
      }
      if ((o instanceof LongStack)){
         LinkedList comparatus = ((LongStack) o).stack;
         if (stack.size() != comparatus.size()){
            return false;
         }
         return comparatus.equals(stack);
      }
      return false;
   }

   @Override
   public String toString() {
      StringBuilder outputString = new StringBuilder();
      for (Long aLong : stack) {
         outputString.insert(0, aLong);
      }
      return outputString.toString();
   }

   public static long interpret (String pol) {
      if (pol.trim().equals("")){
         throw new RuntimeException("Empty expression");
      }

      LongStack polish = new LongStack();
      String[] polishElements = pol.split(" ");

      if (polishElements.length <= 2){
         return Long.parseLong(polishElements[0]);
      }

      int arithmeticsCounter = 0;
      int counter = 0;
      for (String element : polishElements) {
         element = element.trim();
         if(!element.equals("")){
            if (element.equals("+") || element.equals("-") || element.equals("/") || element.equals("*")){
               arithmeticsCounter ++;
            }
            else if (!(element.equals("SWAP") || element.equals("ROT"))){counter ++;}
         }
      }
      if (counter > arithmeticsCounter + 1){
         throw new RuntimeException("Too many numbers in expression " + pol );
      }

      if (counter < arithmeticsCounter + 1){
         throw new RuntimeException("Cannot perform " + polishElements[polishElements.length-1] + " in expression " + pol);
      }

      for (String polishElement : polishElements) {
         polishElement = polishElement.trim();
         if(polishElement.equals("")){
            continue;
         }
         try {
            polish.push(Long.parseLong(polishElement));
         }
         catch (NumberFormatException e) {
            if (!(polishElement.equals("SWAP") || polishElement.equals("ROT") || polishElement.equals("+") || polishElement.equals("-") || polishElement.equals("/") || polishElement.equals("*"))){
               throw new RuntimeException("Illegal symbol " + polishElement + " in expression " + pol);
            }
            if (polishElement.equals("SWAP")) {
               try {
                  long firstNumber = polish.pop();
                  long secondNumber = polish.pop();
                  polish.push(firstNumber);
                  polish.push(secondNumber);
               }
               catch (RuntimeException E){
                  throw new RuntimeException("NOt enough operators!");
               }
            }
            if (polishElement.equals("ROT")){
               try {
                  long firstNumber = polish.pop(); //2
                  long secondNumber = polish.pop(); //5
                  long thirdNumber = polish.pop(); //9
                  polish.push(secondNumber); //5
                  polish.push(firstNumber);//2
                  polish.push(thirdNumber); //9
               }
               catch (RuntimeException E){
                  throw new RuntimeException("NOt enough operators!");
               }
            }
            if (polishElement.equals("+") || polishElement.equals("-") || polishElement.equals("/") || polishElement.equals("*")){
               polish.op(polishElement);
            }
         }
      }
      return polish.pop();
   }
}